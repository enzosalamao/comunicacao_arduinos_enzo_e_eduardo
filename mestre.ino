#include "CRC16.h"
#include "CRC.h"

#define End_mestre 0x01
#define End_1escravo 0x02
#define End_2escravo 0x03
#define Flag 0x7e

char str[] =  "Bom";
uint8_t tamanho;
char str1[] =  "Salve";
uint8_t tamanho1;
char str2[] =  "Oi";
uint8_t tamanho2;
char str3[] =  "Flamengo";
uint8_t tamanho3;
CRC16 crc;

void converterStringEmArrayDeBytes(const char *string) {  // Função para converter uma string em um array de bytes
  
  int tamanho = strlen(string);                           // Calcula o tamanho da string

  int *dados = new int[tamanho];                          // Cria um array de bytes para armazenar a string (new é para inicializar o ponteiro)

  for (int i = 0; i < tamanho; i++) {                     // Converte a string em um array de bytes
    dados[i] = string[i];
    Serial.write(dados[i]);                               // Envia para o escravo os dados
  }
}

uint16_t test()                                           // Calcula o crc da mensagem 1
{ 
  uint16_t crc;
  crc =  (calcCRC16((uint8_t *) str, tamanho));           // Polinomio divisor: x^16 + 1
  return crc;
}

uint16_t test1()                                          // Calcula o crc da mensagem 2
{ 
  uint16_t crc1;
  crc1 =  (calcCRC16((uint8_t *) str1, tamanho1));        // Polinomio divisor: x^16 + 1
  return crc1;
}

uint16_t test2()                                          // Calcula o crc da mensagem 3
{ 
  uint16_t crc2;
  crc2 =  (calcCRC16((uint8_t *) str2, tamanho2));        // Polinomio divisor: x^16 + 1
  return crc2;
}

uint16_t test3()                                          // Calcula o crc da mensagem 4
{ 
  uint16_t crc3;
  crc3 =  (calcCRC16((uint8_t *) str3, tamanho3));        // Polinomio divisor: x^16 + 1
  return crc3;
}

// Função para enviar informações para o Arduino escravo
void enviar_informacoes(int flag, uint8_t endereco, int controle, const char* dados, uint16_t crc) {
  Serial.write(flag);
  Serial.write(endereco); 
  Serial.write(controle);
  converterStringEmArrayDeBytes(dados);
  byte primeiroByte = crc >> 8;                           // Desloca os 8 bits mais significativos para a direita
  byte segundoByte = crc & 0xFF;                          // Faz um AND com 256 ficando apenas os 8 bits menos significativos
  Serial.write(primeiroByte);
  Serial.write(segundoByte);
  Serial.write(flag);
}

void setup() {

  Serial.begin(9600);                                      // Configura a velocidade de transmissão

  // ESCRAVO 1

  int Controle_arduino_1 = 0b00001000;
  int Controle_arduino_1_errado = 0b00001111;

  // Cálculo e verificação do CRC
  tamanho = (sizeof(str) - 1);
  tamanho1 = (sizeof(str1) - 1);
  uint16_t crc = test();
  //Serial.print(crc, HEX);
  uint16_t crc1 = test1();
  //Serial.print(crc1, HEX);

  delay(2000);
  enviar_informacoes(Flag,End_1escravo, Controle_arduino_1_errado, str, crc);
  delay(8000);
  while (Serial.available() > 0){
    char checar_flag = Serial.read();
    if (checar_flag == Flag){
      String mensagem = Serial.readStringUntil(Flag);
      if (mensagem == "i"){
        enviar_informacoes(Flag,End_1escravo, Controle_arduino_1, str, crc);
        delay(8000);
      }
    }
  }
  Controle_arduino_1 = 0b00011001;
  enviar_informacoes(Flag,End_1escravo, Controle_arduino_1, str1, crc1);
  delay(8000);
  while (Serial.available() > 0){
    char checar_flag = Serial.read();
    if (checar_flag == Flag){
      String mensagem = Serial.readStringUntil(Flag);
      if (mensagem == "i"){
        enviar_informacoes(Flag,End_1escravo, Controle_arduino_1, str1, crc1);
        delay(8000);
      }
    }
  }
  Controle_arduino_1 = 0b00101010;
  enviar_informacoes(Flag,End_1escravo, Controle_arduino_1, str, crc1);
  delay(8000);
  while (Serial.available() > 0){
    char checar_flag = Serial.read();
    if (checar_flag == Flag){
      String mensagem = Serial.readStringUntil(Flag);
      if (mensagem == "i"){
        enviar_informacoes(Flag,End_1escravo, Controle_arduino_1, str, crc);
        delay(8000);
      }
    }
  }


  // ESCRAVO 2

  int Controle_arduino_2 = 0b00001000;

  // Cálculo e verificação do CRC
  tamanho2 = (sizeof(str2) - 1);
  tamanho3 = (sizeof(str3) - 1);
  uint16_t crc2 = test2();
  //Serial.print(crc2, HEX);
  uint16_t crc3 = test3();
  //Serial.print(crc3, HEX);

  delay(8000);
  enviar_informacoes(Flag,End_2escravo, Controle_arduino_2, str2, crc2);
  delay(8000);
  Controle_arduino_2 = 0b00011001;
  enviar_informacoes(Flag,End_2escravo, Controle_arduino_2, str3, crc2);
  delay(8000);
  if("i" == "i"){
    enviar_informacoes(Flag,End_2escravo, Controle_arduino_2, str3, crc3);
  }

}

void loop() {
}
