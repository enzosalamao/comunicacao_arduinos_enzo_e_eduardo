#include "CRC16.h"
#include "CRC.h"

#define Flag 0x7e

uint8_t a;

int teste(int j){
  if(j == 0){
    return 0b00001000;
	}
  if(j == 1){
    return 0b00011001;
	}
  if(j == 2){
    return 0b00101010;
	}
  if(j == 3){
    return 0b00111011;
	}
  if(j == 4){
    return 0b01001100;
	}
  if(j == 5){
    return 0b01011101;
	}
  if(j == 6){
    return 0b01101110;
	}
  if(j == 7){
    return 0b01111111;
	}
}


void setup() {
  Serial.begin(9600);                                               // Configura a velocidade de transmissão
  int a = 0;
}

void loop() {
  while (Serial.available()) {
    delay(500);
    byte checar_flag = Serial.read();
    if (checar_flag == 0x7e) {
      delay(500);
      byte checar_endereco = Serial.read();
      if (checar_endereco == 0x03){
        delay(500);
        byte checar_controle = Serial.read();
        if (checar_controle == teste(a)){
          delay(500);
          int *dados = nullptr;                                     // Declara um ponteiro vazio
          int tamanhoDados = 0;                                     // Inicializa a variável tamanhoDados (Para quantos bytes terão até a flag final)
          while (Serial.available() && Serial.peek() != 0x7e) {    
            tamanhoDados++;
            dados = (int *)realloc(dados, tamanhoDados * sizeof(int));   // Realocando dinamicamente na memória | realloc acrescenta em dados o tamanho de int vezes o tamnho dos dados
            dados[tamanhoDados - 1] = Serial.read();
            //Serial.print(dados[tamanhoDados - 1]);
          }
        
          char mensagem[(tamanhoDados - 2)];
          for (int i = 0; i < (tamanhoDados - 2); i++) {
                mensagem[i] = dados[i];
              }
          Serial.print(mensagem);

          uint16_t crcCalculado = calcCRC16((uint8_t *)mensagem, (tamanhoDados - 2));               // Calcula o CRC dos dados recebidos
          //Serial.print(crcCalculado, HEX);

          //Lê o CRC
          uint16_t crc = ((uint16_t)dados[tamanhoDados - 2] << 8) | dados[tamanhoDados - 1];        // Lê o CRC que foi enviado
          //Serial.print(crc, HEX);

          if(crcCalculado == crc){ 
            delay(500);
            a = a + 1;
            Serial.println();
            Serial.println("Mensagem enviada: ");
            Serial.print(mensagem);
            Serial.println();
            Serial.write("1");
            Serial.println();
          }
          else{
            delay(500);
            Serial.println();
            Serial.println("CRC incorreto! Reenvie a Mensagem!");
            Serial.println();
            Serial.write(Flag);
            Serial.write("i");
            Serial.write(Flag);
          }
        }
        else{
          delay(500);
          Serial.println();
          Serial.println("CONTROLE incorreto! Reenvie a Mensagem!");
          Serial.println();
          Serial.write(Flag);
          Serial.write("i");
          Serial.write(Flag);
        }
      }
    }
  }
}
